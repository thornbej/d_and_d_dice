#!/bin/python3

#a D&D dice rolling calculator, using no of sides on a dice(sided_dice), and no. of rolls to be done (dice_rolls). 

import random

dice_roll = input("please enter your D&D roll e.g. '1d20':")
print(dice_roll)
#it can't be too long!
if len(dice_roll) > 4:
        print("too long a number - sorry")
        exit
#it can't be too short
if len(dice_roll) < 3:
        print("too short a number - sorry")
        exit
if len(dice_roll) == 0:
        print("you didn't enter a value!")
#it can be either 4 chars long e.g 1d20
if len(dice_roll) == 4:
    dice_rolls,sided_dice = dice_roll.split("d")
    print(f"there will be {dice_rolls} dice roll(s) of a {sided_dice} sided dice")
    exit
# it can be 3 chars long e.g. 3d6
if len(dice_roll) == 3:
    dice_rolls,sided_dice = dice_roll.split("d")
    print(f"there will be {dice_rolls} dice roll(s) of a {sided_dice} sided dice")
    exit

#choose a random number between 1 and no.of sides on the dice, multiple that by the times the dice gets to be rolled
def roll(sided_dice,dice_rolls):
    sided_dice = int(sided_dice)
    dice_rolls = int(dice_rolls)
    return [random.randint(1,sided_dice) for i in range(dice_rolls)]
results = roll(sided_dice,dice_rolls)

#print the results
print(results)

#The End :)
exit
